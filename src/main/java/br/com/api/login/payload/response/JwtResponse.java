package br.com.api.login.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JwtResponse{

  private static final String type="Bearer";

  private Long id;
  private String token;
  private String username;
  private String email;
  private List<String> roless;

  public JwtResponse(String token, Long id, String username, String email, List<String> roless){
    this.id=id;
    this.token=token;
    this.username=username;
    this.email=email;
    this.roless=roless;
  }
}
