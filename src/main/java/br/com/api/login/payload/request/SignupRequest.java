package br.com.api.login.payload.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class SignupRequest{

  @NotBlank
  @Size(min=3, max=20)
  private String username;

  @NotBlank
  @Size(max=100)
  @Email
  private String email;

  @NotBlank
  @Size(min=6, max=100)
  private String password;

  private Set<String> role;
}
