package br.com.api.login.security.services;

import br.com.api.login.model.User;
import br.com.api.login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

  private final UserRepository repository;

  @Autowired
  public UserDetailsServiceImpl(UserRepository repository){
    this.repository=repository;
  }

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
    User user=repository
        .findByUsername(username)
        .orElseThrow(()->new UsernameNotFoundException("User Not Found With username"+username));
    return UserDetailsImpl.build(user);
  }
}
