package br.com.api.login.controller;

import br.com.api.login.model.ERole;
import br.com.api.login.model.Role;
import br.com.api.login.model.User;
import br.com.api.login.payload.request.SignupRequest;
import br.com.api.login.payload.request.SinginRequest;
import br.com.api.login.payload.response.JwtResponse;
import br.com.api.login.payload.response.MessageResponse;
import br.com.api.login.repository.RoleRepository;
import br.com.api.login.repository.UserRepository;
import br.com.api.login.security.jwt.JwtUtils;
import br.com.api.login.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController{

  private final AuthenticationManager authenticationManager;
  private final UserRepository userRepository;
  private final RoleRepository roleRepository;
  private final PasswordEncoder encoder;
  private final JwtUtils utils;

  private static final String ROLE_NOT_FOUND = "Error: Role is not found.";

  @Autowired
  public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository,
                        RoleRepository roleRepository, PasswordEncoder encoder, JwtUtils utils){
    this.authenticationManager = authenticationManager;
    this.userRepository = userRepository;
    this.roleRepository = roleRepository;
    this.encoder = encoder;
    this.utils = utils;
  }

  @PostMapping("/signin")
  public ResponseEntity<JwtResponse> authenticatedUser(@Valid @RequestBody SinginRequest loginRequest){
    Authentication authentication = authenticationManager.authenticate(
      new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    SecurityContextHolder
      .getContext()
      .setAuthentication(authentication);
    String jwt = utils.generateJwtToken(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails
      .getAuthorities()
      .stream()
      .map(GrantedAuthority::getAuthority)
      .collect(Collectors.toList());

    return ResponseEntity.ok(
      new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles));
  }

  @PostMapping("/signup")
  public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signupRequest){
    boolean existsUsername = userRepository.existsByUsername(signupRequest.getUsername());
    if(existsUsername){
      return ResponseEntity
        .badRequest()
        .body(new MessageResponse("Error: Username is already taken!"));
    }

    boolean existsEmail = userRepository.existsByEmail(signupRequest.getEmail());
    if(existsEmail){
      return ResponseEntity
        .badRequest()
        .body(new MessageResponse("Error: Email is already in use!"));
    }

    User user = new User(signupRequest.getUsername(), signupRequest.getEmail(),
      encoder.encode(signupRequest.getPassword()));
    Set<String> strRoles = signupRequest.getRole();
    Set<Role> roles = new HashSet<>();

    if(strRoles == null){
      Role userRole = roleRepository
        .findByName(ERole.ROLE_USER)
        .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND));
      roles.add(userRole);
    }else{
      strRoles.forEach(role -> {
        switch(role){
          case "admin":
            Role adminRole = roleRepository
              .findByName(ERole.ROLE_ADMIN)
              .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND));
            roles.add(adminRole);
            break;
          case "mod":
            Role modRole = roleRepository
              .findByName(ERole.ROLE_MODERATOR)
              .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND));
            roles.add(modRole);
            break;
          default:
            Role userRole = roleRepository
              .findByName(ERole.ROLE_USER)
              .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND));
            roles.add(userRole);
        }
      });
    }

    user.setRoles(roles);
    userRepository.save(user);

    return ResponseEntity.ok(new MessageResponse("User registered Successfully!"));
  }
}
