package br.com.api.login.model;

public enum ERole{
  ROLE_USER, ROLE_MODERATOR, ROLE_ADMIN
}
