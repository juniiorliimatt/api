package br.com.api.login.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="roles")
@NoArgsConstructor
@Getter
public class Role{

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  @Enumerated(EnumType.STRING)
  @Column(length=20)
  private ERole name;

  public Role(ERole name){
    this.name=name;
  }

  public void setName(ERole name){
    this.name=name;
  }
}
