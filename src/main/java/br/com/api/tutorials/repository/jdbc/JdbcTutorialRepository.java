package br.com.api.tutorials.repository.jdbc;

import br.com.api.tutorials.model.Tutorial;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcTutorialRepository implements TutorialRepository{

  private final JdbcTemplate template;
  private String sql="";

  public JdbcTutorialRepository(JdbcTemplate template){
    this.template=template;
  }

  @Override
  public Integer save(Tutorial tutorial){
    sql="INSERT INTO tutorials (title, description, published) VALUES (?, ?, ?)";
    return template.update(sql, tutorial.getTitle(), tutorial.getDescription(), tutorial.getPublished());
  }

  @Override
  public Integer update(Tutorial tutorial){
    sql="UPDATE tutorials SET title=?, description=?, published=? where id=?";
    return template.update(sql, tutorial.getTitle(), tutorial.getDescription(), tutorial.getPublished(), tutorial.getId());
  }

  @Override
  public Tutorial findById(Long id){
    try{
      sql="SELECT * FROM tutorials where id=?";
      return template.queryForObject(sql, BeanPropertyRowMapper.newInstance(Tutorial.class), id);
    }catch(IncorrectResultSizeDataAccessException e){
      return null;
    }
  }

  @Override
  public Integer deleteById(Long id){
    sql="DELETE FROM tutorials WHERE id=?";
    return template.update(sql, id);
  }

  @Override
  public List<Tutorial> findAll(){
    sql="SELECT * FROM tutorials";
    return template.query(sql, BeanPropertyRowMapper.newInstance(Tutorial.class));
  }

  @Override
  public List<Tutorial> findByPublished(Boolean published){
    sql="SELECT * FROM tutorials WHERE published=?";
    return template.query(sql, BeanPropertyRowMapper.newInstance(Tutorial.class), published);
  }

  @Override
  public List<Tutorial> findByTitleContaining(String title){
    sql="SELECT * FROM tutorials WHERE title ILIKE '%"+title+"%'";
    return template.query(sql, BeanPropertyRowMapper.newInstance(Tutorial.class));
  }

  @Override
  public Integer deleteAll(){
    return template.update("DELETE FROM tutorials");
  }
}
