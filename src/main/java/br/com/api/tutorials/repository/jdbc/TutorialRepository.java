package br.com.api.tutorials.repository.jdbc;

import br.com.api.tutorials.model.Tutorial;

import java.util.List;

public interface TutorialRepository{
  Integer save(Tutorial tutorial);
  Integer update(Tutorial tutorial);
  Tutorial findById(Long id);
  Integer deleteById(Long id);
  List<Tutorial> findAll();
  List<Tutorial> findByPublished(Boolean published);
  List<Tutorial> findByTitleContaining(String title);
  Integer deleteAll();
}
