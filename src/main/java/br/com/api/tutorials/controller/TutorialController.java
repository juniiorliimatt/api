package br.com.api.tutorials.controller;

import br.com.api.tutorials.model.Tutorial;
import br.com.api.tutorials.repository.jdbc.TutorialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins="${APP_LINK}")
@RestController
@RequestMapping("/api/tutorials")
public class TutorialController{

  private final TutorialRepository repository;

  @Autowired
  public TutorialController(TutorialRepository repository){
    this.repository=repository;
  }

  @GetMapping
  public ResponseEntity<List<Tutorial>> getAllTutorials(@RequestParam(required=false) String title){
    try{
      List<Tutorial> tutorials=new ArrayList<>();

      if(title==null){
        repository.findAll().forEach(tutorials::add);
      }else{
        repository.findByTitleContaining(title).forEach(tutorials::add);
      }

      if(tutorials.isEmpty()){
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(tutorials, HttpStatus.OK);
    }catch(Exception e){
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/published")
  public ResponseEntity<List<Tutorial>> findByPublished(
      @RequestParam(required=false, defaultValue="true") Boolean published){
    try{
      List<Tutorial> tutorials=new ArrayList<>();
      if(published){
        repository.findByPublished(true).forEach(tutorials::add);
      }
      repository.findByPublished(published).forEach(tutorials::add);

      if(tutorials.isEmpty()){
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }
      return new ResponseEntity<>(tutorials, HttpStatus.OK);
    }catch(Exception e){
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/{id}")
  public ResponseEntity<Tutorial> getTutorialById(@PathVariable Long id){
    Tutorial tutorial=repository.findById(id);

    if(tutorial!=null){
      return new ResponseEntity<>(tutorial, HttpStatus.OK);
    }

    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @PostMapping
  public ResponseEntity<String> createTutorial(@RequestBody Tutorial tutorial){
    try{
      repository.save(new Tutorial(tutorial.getTitle(), tutorial.getDescription(), false));
      return new ResponseEntity<>("Tutorial was created successfully.", HttpStatus.CREATED);
    }catch(Exception e){
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/{id}")
  public ResponseEntity<String> updateTutorial(@PathVariable("id") Long id, @RequestBody Tutorial tutorial){
    Tutorial _tutorial=repository.findById(id);

    if(_tutorial!=null){
      _tutorial.setId(id);
      _tutorial.setTitle(tutorial.getTitle());
      _tutorial.setDescription(tutorial.getDescription());
      _tutorial.setPublished(tutorial.getPublished());
      repository.update(_tutorial);
      return new ResponseEntity<>("Tutorial was update successfully.", HttpStatus.OK);
    }
    return new ResponseEntity<>("Cannot find tutorial with id="+id, HttpStatus.NOT_FOUND);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<String> deleteTutorial(@PathVariable("id") Long id){
    try{
      int result=repository.deleteById(id);
      if(result==0){
        return new ResponseEntity<>("Cannot find Tutorial with id: "+id, HttpStatus.NOT_FOUND);
      }
      return new ResponseEntity<>("Tutorial was deleted successfully.", HttpStatus.OK);
    }catch(Exception e){
      return new ResponseEntity<>("Cannot delete tutorial", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @DeleteMapping
  public ResponseEntity<String> deleteAllTutorials(){
    try{
      int numRows=repository.deleteAll();
      return new ResponseEntity<>("Deleted "+numRows+" Tutorial(s) successfully,", HttpStatus.OK);
    }catch(Exception e){
      return new ResponseEntity<>("Cannot delete tutorials.", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
