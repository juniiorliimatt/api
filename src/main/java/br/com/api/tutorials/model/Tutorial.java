package br.com.api.tutorials.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="tutorials")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Tutorial{

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String title;
  private String description;
  private Boolean published;

  public Tutorial(String title, String description, Boolean published){
    this.title=title;
    this.description=description;
    this.published=published;
  }
}
