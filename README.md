# API's RESTFull
#### Hospedada no Heroku
API - [heroku](https://api-oojuniin.herokuapp.com/)

### Tutorials
Frontend - [vercel](https://tutorial-app.vercel.app/tutorials)
Repositorio Frontend - [GitLab](https://gitlab.com/oojuniin/tutorial-app)

### Buscar todos os tutoriais
```[GET] https://api-oojuniin.herokuapp.com/api/tutorials```

### Buscar por id
```[GET] https://api-oojuniin.herokuapp.com/api/tutorials/{id}```

### Buscar por publicada ou não
```[GET] https://api-oojuniin.herokuapp.com/api/tutorials/published?published=false```

### Buscar por título
```[GET] https://api-oojuniin.herokuapp.com/api/tutorials?title=title 2```

### Salvar um novo tutorial
```[POST] https://api-oojuniin.herokuapp.com/api/tutorials```
```JSON
{
  "title":"Junior",
  "description":"Teste do Junior"
}
```

### Atualizando um novo
```[PUT] https://api-oojuniin.herokuapp.com/api/tutorials/{id}```
```JSON
{
  "title":"title 1",
  "description":"description 1",
  "published":true
}
```

### Deletar todos
```[DELETE] https://api-oojuniin.herokuapp.com/api/tutorials```

### Deletar por id
```[DELETE] https://api-oojuniin.herokuapp.com/api/tutorials/{id}```
